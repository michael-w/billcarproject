﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Billcar.Core.Models;

namespace Billcar.DAL.Context
{
    public class PersonContext
    {
        public partial class BazaDanychMWEntities1 : DbContext
        {
            public BazaDanychMWEntities1()
                : base("name=BazaDanychMWEntities1")
            {
                //string test = AppDomain.CurrentDomain.BaseDirectory;
                //////if(test.IndexOf("TestBackend", StringComparison.Ordinal)<=0)
                //if (test.IndexOf("UnitTests", StringComparison.Ordinal) <= 0)
                //    this.Configuration.LazyLoadingEnabled = false;  // Ważne, inaczej powstanie nieskończona pętla JSON...
            }

            protected override void OnModelCreating(DbModelBuilder modelBuilder)
            {
                throw new UnintentionalCodeFirstException();
            }

            // Nasza funkcja która mówi, że funkcja Persons zwraca z bazy danych klasę typu Person [tzn bazę danych tego typu]
            public virtual DbSet<Person> Persons { get; set; }
            public virtual DbSet<Section> Sections { get; set; }
            public virtual DbSet<Poster> Posters { get; set; }
            public virtual DbSet<Token> Tokens { get; set; }
            public virtual DbSet<Mail> Mails { get; set; }
            public virtual DbSet<Group> Groups { get; set; }
        }
    }

}