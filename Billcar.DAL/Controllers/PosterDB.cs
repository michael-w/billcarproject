﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Billcar.DAL.DB
{
    public partial class DatabaseController : DB
    {
        public List<Poster> GetPostersDB()
        {
            try
            {
                List<Poster> posters = db.Posters.ToList();
                return posters;
            }
            catch
            {
                return null;
            }
        }

        public List<Poster> GetPostersByPerson(Person p)
        {
            //List<Section> sections = new List<Section>();
            //var personFetch =
            //(from pfetch in db.Persons
            // where pfetch.Id == p.Id
            // select pfetch).All();
            List<Poster> posters = db.Posters.Where(o => o.AuthorID == p.Id).ToList();
            return posters;
        }
        public bool PosterCreate(Poster pstr)
        {
            try
            {
                db.Posters.Add(pstr);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Poster PosterRead(int id)
        {
            Poster pstr = db.Posters.FirstOrDefault(o => o.Id == id);
            return pstr;
        }
        public bool PosterUpdate(Poster pstr)
        {
            try
            {
                var query =
                     from poster in db.Posters
                     where poster.Id == pstr.Id
                     select poster;
                foreach (Poster poster in query)
                {
                    poster.Direction = pstr.Direction;
                    poster.From = pstr.From;
                    poster.To = pstr.To;
                    poster.TransportCurrSize = pstr.TransportCurrSize;
                    poster.TransportMaxSize = pstr.TransportMaxSize;
                    poster.TransportDate = pstr.TransportDate;
                }
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool PosterDelete(Poster pstr)
        {
            try
            {
                var posterFetch =
                (from pfetch in db.Posters
                 where pfetch.Id == pstr.Id
                 select pfetch).First();
                db.Posters.Remove(posterFetch);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}