﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

using static Billcar.DAL.Context.PersonContext;


namespace Billcar.DAL.DB
{
    public partial class DatabaseController : DB
    {
        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Zwraca wszystkie osoby z tabelki Person!
        /// TEST
        /// </summary>
        public List<Person> GetPeopleDB()
        {
            List<Person> people = db.Persons.ToList();
            // Zwracamy dane i nic więcej
            return people;
        }
        public Person GetPersonByEmail(string mail)
        {
            Person p = db.Persons.FirstOrDefault(o => o.Email == mail);
            return p;
        }
        public bool SetPersonByPerson(Person p)
        {
            db.Persons.Add(p);
            db.SaveChanges();
            return true;
        }
        public Person GetPersonByEmailPassword(string email, string password)
        {
            Person p = db.Persons.FirstOrDefault(o => o.Email == email && o.Password == password);
            return p;
        }

        public bool PersonCreate(Person p)
        {
            try
            {
                db.Persons.Add(p);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Token ReturnToken(Token t)
        {
           return db.Tokens.FirstOrDefault(o => o.TokenValue == t.TokenValue);

        }
        public bool TokenCreate(Token t)
        {
           db.Tokens.Add(t);
           db.SaveChanges();
           return true;
           
        }
        public bool TokemRemove(Token t)
        {
            try
            {
                Token p = db.Tokens.FirstOrDefault(o => o.TokenValue == t.TokenValue);
                if (p != null) db.Tokens.Remove(p);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool TokenCheck(Token t)
        {
            try
            {
                Token p = db.Tokens.FirstOrDefault(o => o.TokenValue == t.TokenValue);
                return p != null;
            }
            catch
            {
                return false;
            }
        }
        public Person PersonRead(int id)
        {
            Person p = db.Persons.FirstOrDefault(o => o.Id == id);
            return p;
        }
        public bool PersonUpdate(Person p)
        {
            try
            {
                Person person = db.Persons.FirstOrDefault(o => o.Id == p.Id);
                if (person == null) return false;
                person.Flags = p.Flags;
                person.Section = p.Section;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool PersonDelete(Person p)
        {
            try
            {
                var personFetch =
                (from pfetch in db.Persons
                 where pfetch.Id == p.Id
                 select pfetch).First();
                db.Persons.Remove(personFetch);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}