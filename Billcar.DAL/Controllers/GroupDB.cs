﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static Billcar.DAL.Context.PersonContext;

namespace Billcar.DAL.DB
{
    public partial class DatabaseController : DB
    {
        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Zwraca wszystkie osoby z tabelki Person!
        /// TEST
        /// </summary>
        /// 

        public List<Group> GetGroupsDB()
        {
            List<Group> groups = db.Groups.ToList();
            return groups;
        }

        public bool CheckIfUserBelongsToPoster(int perID, int postID)
        {
            Group group = db.Groups.Where(o => o.PersonID == perID && o.PosterID == postID).FirstOrDefault();
            if (group != null)
                return true;
            else
                return false;
        }

        public List<Group> GetGroupsByPosterID(int id)
        {
            List<Group> groups = db.Groups.Where(o => o.PosterID == id).ToList();
            return groups;
        }

        public Group GetGroupByPersonIDAndGroupID(int perId, int postId)
        {
            Group group = db.Groups.Where(o => o.PersonID == perId && o.PosterID == postId).FirstOrDefault();
            return group;
        }

        public bool GroupCreate(Group g)
        {
            try
            {
                db.Groups.Add(g);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public Group GroupRead(int id)
        {
            Group g = db.Groups.FirstOrDefault(o => o.Id == id);
            return g;
        }

        public bool GroupUpdate(Group g)
        {
            try
            {
                var query =
                     from grp in db.Groups
                     where grp.Id == g.Id
                     select grp;
                foreach (Group grp in query)
                {
                    grp.PersonID = g.PersonID;
                }
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool GroupDelete(Group g)
        {
            try
            {
                var groupFetch =
                (from sfetch in db.Groups
                 where sfetch.Id == g.Id
                 select sfetch).ToList();
                foreach(var grp in groupFetch)
                {
                    db.Groups.Remove(grp);
                }
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}