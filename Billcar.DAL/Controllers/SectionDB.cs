﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static Billcar.DAL.Context.PersonContext;


namespace Billcar.DAL.DB
{
    public partial class DatabaseController : DB
    {
        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Zwraca wszystkie osoby z tabelki Person!
        /// TEST
        /// </summary>
        public List<Section> GetSectionsDB()
        {
            List<Section> sections = db.Sections.ToList();
            return sections;
        }
        public Section GetSectionByCity(string city)
        {
            Section s = db.Sections.FirstOrDefault(o => o.CityName == city);
            return s;
        }
        public bool SectionCreate(Section s)
        {
            try
            {
                db.Sections.Add(s);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Section SectionRead(int id)
        {
            Section s = db.Sections.FirstOrDefault(o => o.Id == id);
            return s;
        }
        public bool SectionUpdate(Section s)
        {
            try
            {
                var query =
                     from sect in db.Sections
                     where sect.Id == s.Id
                     select sect;
                foreach (Section sect in query)
                {
                    sect.CityName = s.CityName;
                }
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool SectionDelete(Section s)
        {
            try
            {
                var sectrionFetch =
                (from sfetch in db.Sections
                 where sfetch.Id == s.Id
                 select sfetch).First();
                db.Sections.Remove(sectrionFetch);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}