﻿using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

using static Billcar.DAL.Context.PersonContext;


namespace Billcar.DAL.DB
{
    public class DB
    {
        // Inicjalizacja połączenia do bazy danych w tej warstwie 
        internal BazaDanychMWEntities1 db = new BazaDanychMWEntities1();
        //Funkcja publiczna która zwróci rekordy Persons gdy inna warstwa wywoła tą warstwę
        /// <summary>
        /// Zwraca wszystkie osoby z tabelki Person!
        /// TEST
        /// </summary>
        public DB()
        {
                // Sciezka projektu... niestety my chcemy sciezke solucji!
                var dir = AppDomain.CurrentDomain.BaseDirectory.ToString();
                // Cofamy się z projektu
                dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\"));
                if (dir.IndexOf("UnitTests", StringComparison.Ordinal) > 0)
                {
                    dir = System.IO.Path.GetFullPath(System.IO.Path.Combine(dir, "..\\..\\"));
                }
                // Bierzemy dodajemy do solucji sciezke do bazy danych i utworzomy ze to tutaj jest folder danych
                dir = dir + "Billcar.DAL\\App_Data\\";
                //.SetData("DataDirectory", AppDomain.CurrentDomain.ToString());
                AppDomain.CurrentDomain.SetData("DataDirectory", dir);
        }
    }
}