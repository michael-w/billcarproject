﻿using Billcar.Core.Models;
using System.Collections.Generic;
using System.Linq;

using static Billcar.DAL.Context.PersonContext;

namespace Billcar.DAL.DB
{
    public partial class DatabaseController : DB
    {
        public List<Mail> GetMailsDB()
        {
            List<Mail> mails = db.Mails.ToList();
            return mails;
        }
        public bool MailCreate(Mail m)
        {
            try
            {
                db.Mails.Add(m);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public Mail MailRead(int id)
        {
            Mail m = db.Mails.FirstOrDefault(o => o.Id == id);
            return m;
        }
        public bool MailUpdate(Mail m)
        {
            try
            {
                var query =
                     from mail in db.Mails
                     where mail.Id == m.Id
                     select mail;
                foreach (Mail mail in query)
                {
                    mail.Subject = m.Subject;
                    mail.Body = m.Body;
                }
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool MailDelete(Mail m)
        {
            try
            {
                var mailFetch =
                (from mfetch in db.Mails
                 where mfetch.Id == m.Id
                 select mfetch).First();
                db.Mails.Remove(mailFetch);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}