﻿using Billcar.Core.Models;
using Billcar.DAL.DB;
using System.Collections.Generic;

namespace Billcar.BL
{
    public class SectionBL
    {
        private readonly DatabaseController _db;
        public SectionBL()
        {
            _db = new DatabaseController();
        }


        public ValidationResponse Create(Section s)
        {
            ValidationResponse response;
            response = CityValid(s.CityName);
            if(response.Successful)
            {
                if (_db.SectionCreate(s))
                    response = new ValidationResponse(true, "Success!");
                else
                    response = new ValidationResponse(false, "Error with database!");
            }
            return response;
        }
        public Section Read(int id)
        {
            return _db.SectionRead(id);
        }

        public List<ValidationResponse> UpdateManySection(Section[] sections)
        {
            List<ValidationResponse> responses = new List<ValidationResponse>();
            if (sections == null)
            {
                responses.Add(new ValidationResponse(false, "There is no sections!"));
                return responses;
            }
            foreach (Section section in sections)
            {
                responses.Add(Update(section));
            }
            return responses;
        }

        public ValidationResponse Update(Section s)
        {
            ValidationResponse response;
            if (_db.SectionUpdate(s))
                response = new ValidationResponse(true, "Success!");
            else
                response = new ValidationResponse(false, "Error with database!");
            return response;
        }

        public List<string> DeleteManySections(int[] ids)
        {
            List<string> errorMessages = new List<string>();
            for (int i = 0; i < ids.Length; i++)
            {
                Section section = Read(ids[i]);
                ValidationResponse response = Delete(section);
                if (response.Information != null && response.Information != "")
                {
                    errorMessages.Add("#" + section.Id + ":" + response.Information);
                }
            }
            return errorMessages;
        }

        public ValidationResponse Delete(Section s)
        {
            if(s.Person.Count>0)
                return new ValidationResponse(false, "Oddział ma przypisanych użytkowników.");
            if(_db.SectionDelete(s)==false)
                return new ValidationResponse(false, "Nie udało się usunąć oddziału.");
            return new ValidationResponse(true);
        }

        public ValidationResponse CityValid(string city)
        {
            ValidationResponse response;
            if(string.IsNullOrEmpty(city))
            {
                response = new ValidationResponse(false, "City name is null or empty");
            }
            else if (_db.GetSectionByCity(city) != null)
            {
                response = new ValidationResponse(false, "Section with that city name already exists!");
            }
            else
            {
                response = new ValidationResponse(true);
            }
            return response;
        }




        }
}