﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using static Billcar.DAL.Context.PersonContext;

namespace Billcar.BL
{
    public class PersonBL
    {
        private readonly DatabaseController _db;
        public PersonBL()
        {
            _db = new DatabaseController();
        }

        public ValidationResponse ChangeUserPassword(Person p, string oldPassword, string newPassword)
        {
            SecurityBL security = new SecurityBL();
            PersonBL connect = new PersonBL();
            string salt = p.Salt;
            ValidationResponse result;

            oldPassword = security.GenerateSHA256Hash(oldPassword, salt);

            if (oldPassword != p.Password)
            {
                return new ValidationResponse(false,"Current password is not correct!)");
            }
            salt = security.CreateSalt(32);
            newPassword = security.GenerateSHA256Hash(newPassword, salt);
            p.Password = newPassword;
            result = Update(p);
            return result;
        }

        public List<Person> GetPeople()
        {
            List<Person> people = _db.GetPeopleDB();
            return people;
        }
        public bool Create(Person p)
        {
            return _db.PersonCreate(p);
        }
        public Person Read(int id)
        {
            return _db.PersonRead(id);
        }
        public ValidationResponse Update(Person p)
        {
            ValidationResponse response;
            if (_db.PersonUpdate(p))
            {
                response = new ValidationResponse(true, "Success!");
            }
            else
                response = new ValidationResponse(false, "Error with database!");
            return response;
        }

        public List<ValidationResponse> UpdateManyUsers(Person[] users)
        {
            List<ValidationResponse> responses = new List<ValidationResponse>();
            if (users == null)
            {
                responses.Add(new ValidationResponse(false, "User list is empty!"));
                return responses;
            }
            foreach (Person user in users)
            {
                responses.Add(Update(user));
            }
            return responses;
        }

        public ValidationResponse Delete(Person p)
        {
            ValidationResponse response;
            if (_db.PersonDelete(p))
            {
                response = new ValidationResponse(true, "Success!");
            }
            else
                response = new ValidationResponse(false, "Error with database!");
            return response;
        }

        public List<ValidationResponse> RemovePeople(int[] ids)
        {
            List<ValidationResponse> responses = new List<ValidationResponse>();

            if (ids == null)
            {
                responses.Add(new ValidationResponse(false, "There is no users!"));
                return responses;
            }

            for (int i = 0; i < ids.Length; i++)
            {
                Person man = Read(ids[i]);
                if (man == null)
                {
                    responses.Add(new ValidationResponse(false, "#" + i + " : This user does not exist in database!"));
                }
                ValidationResponse resp = Delete(man);
                responses.Add(resp);
            }
            return responses;
        }


    }
}