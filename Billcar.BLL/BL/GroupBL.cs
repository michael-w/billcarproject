﻿using System.Collections.Generic;
using System.Linq;
using Billcar.Core.Models;
using Billcar.DAL.DB;

namespace Billcar.BL
{
    public class GroupBL
    {
        private readonly DatabaseController _db;

        public GroupBL()
        {
            _db = new DatabaseController();
        }

        public bool Create(Group grp)
        {
            return _db.GroupCreate(grp);
        }
        public Group Read(int id)
        {
            return _db.GroupRead(id);
        }
        public bool Update(Group grp)
        {
            return _db.GroupUpdate(grp);
        }
        public bool Delete(Group grp)
        {
            return _db.GroupDelete(grp);
        }
    }
}