﻿using System;
using System.Collections.Generic;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using System.Threading;

namespace Billcar.BL
{
    public class MailBL
    {
        private readonly DatabaseController _db;
        private readonly int _secs = 3600;
        public MailBL()
        {
            _db = new DatabaseController();
        }
        public MailBL(int secs)
        {
            _db = new DatabaseController();
            this._secs = secs;
        }

        public void StartMailService()
        {
            Thread serverTimerThread = new Thread(new ThreadStart(ServerTimerThread));
            serverTimerThread.Start();
        }

        public void ServerTimerThread()
        {
            while(true)
            {
                DateTime future = DateTime.Now.AddSeconds(_secs);
                while (future > DateTime.Now)
                {
                    Thread.Sleep(1000);
                }
                SendMails();
            }
        }

        public void SendMails()
        {
            Thread sendMailsThread = new Thread(SendMailsThread);
            sendMailsThread.Start();
        }
        public void SendMailsThread()
        {
            PersonBL pBL = new PersonBL();
            List<Mail> mails = GetMails();
            foreach (Mail m in mails)
            {
                EmailSender eS = new EmailSender(pBL.Read(m.ToPersonID).Email,m.Subject,m.Body);
                eS.Send();
                _db.MailDelete(m);
            }
        }
        public List<Mail> GetMails()
        {
            List<Mail> mails = _db.GetMailsDB();
            return mails;
        }
        public bool Create(Mail m)
        {
            return _db.MailCreate(m);
        }
        public Mail Read(int id)
        {
            return _db.MailRead(id);
        }
        public bool Update(Mail m)
        {
            return _db.MailUpdate(m);
        }
        public bool Delete(Mail m)
        {
            return _db.MailDelete(m);
        }
    }
}