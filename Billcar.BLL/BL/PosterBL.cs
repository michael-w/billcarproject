﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Billcar.Core.Models;
using Billcar.DAL.DB;
using static Billcar.DAL.Context.PersonContext;

namespace Billcar.BL
{
    public class PosterBL
    {
        private readonly DatabaseController _db;
        public PosterBL()
        {
            _db = new DatabaseController();
        }

        public ValidationResponse RemovePassengerFromPoster(Person person, Poster poster)
        {
            ValidationResponse response;
            GroupBL gBL = new GroupBL();

            if(CheckIfPersonBelongsToGroup(person.Id,poster.Id).Successful)
            {
                if (!gBL.Delete(GetGroupByPersonIDAndGroupID(person.Id, poster.Id)))
                {
                    response = new ValidationResponse(false, "Error with database! (Group delete)");
                }
                else
                {
                    poster.TransportCurrSize--;
                    if (!Update(poster))
                    {
                        response = new ValidationResponse(false, "Error with database! (Update poster)");
                    }
                    else
                    {
                        response = new ValidationResponse(true, "Success!");
                    }
                }
            }
            else
            {
                response = new ValidationResponse(false, "User does not belong to this poster!");
            }

            return response;
            
        }

        public ValidationResponse AddPassengerToPoster(Person person, Poster poster)
        {
            ValidationResponse response;
            GroupBL gBL = new GroupBL();
            Group group = new Group();
            
            group.PersonID = person.Id;
            group.PosterID = poster.Id;
            
            if(CheckIfPersonBelongsToGroup(person.Id, poster.Id).Successful)
            {
                response = new ValidationResponse(false, "User already belongs to this poster!");
            }
            else
            {
                if (poster.TransportMaxSize > poster.TransportCurrSize)
                {
                    if (!gBL.Create(group))
                    {
                        response = new ValidationResponse(false, "Error with database! (Create group)");
                    }
                    else
                    {
                        poster.TransportCurrSize++;
                        if (!Update(poster))
                        {
                            response = new ValidationResponse(false, "Error with database! (Update poster)");
                        }
                        else
                        {
                            response = new ValidationResponse(true, "Success!");
                        }
                    }
                }
                else
                {
                    response = new ValidationResponse(false, "There is not slots left!");
                }
            }
            return response;
        }

        public ValidationResponse CheckIfPersonBelongsToGroup(int perID, int postID)
        {
            ValidationResponse response = new ValidationResponse();
            response.Successful = _db.CheckIfUserBelongsToPoster(perID, postID);
            if (response.Successful)
                response = new ValidationResponse(true, "User already belongs to this poster!");
            else
                response = new ValidationResponse(false, "User does not belong to this poster!");
            return response;
        }

        public List<Group> GetGroupsByPosterID(int id)
        {
            List<Group> groups = _db.GetGroupsByPosterID(id);
            return groups;
        }

        public Group GetGroupByPersonIDAndGroupID(int perId, int postId)
        {
            Group group = _db.GetGroupByPersonIDAndGroupID(perId, postId);
            return group;
        }

        public List<Poster> GetPosters()
        {
            List<Poster> posters = _db.GetPostersDB();
            return posters;
        }

        public List<Poster> GetPostersByPerson(Person p)
        {
            List<Poster> posters = _db.GetPostersByPerson(p);
            return posters;
        }
        public bool Create(Poster pstr)
        {
            return _db.PosterCreate(pstr);
        }
        public Poster Read(int id)
        {
            return _db.PosterRead(id);
        }
        public bool Update(Poster pstr)
        {
            return _db.PosterUpdate(pstr);
        }
        public bool Delete(Poster pstr)
        {
            return _db.PosterDelete(pstr);
        }

        public int[] PostersCountInfo(Person p)
        {
            List<Poster> posters = GetPosters();

            int[] values = new int[3];
            values[0] = posters.Count;
            values[1] = posters.Count(o => o.AuthorID == p.Id);
            values[2] = p.Poster.Count;

            return values;
        }
    }
}