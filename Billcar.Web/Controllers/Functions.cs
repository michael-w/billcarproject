﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Newtonsoft.Json;
using Billcar.Core.Models;
using Billcar.BL;

namespace WebTest.Controllers
{
    
    static class JSONConvert
    {
        public static string SerializeObject(object objectItem)
        {
            return JsonConvert.SerializeObject(objectItem, Formatting.None,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });
        }



    }

    [RoutePrefix("api")]
    public partial class RootController
    {

        [Route("CheckLogin")]
        [HttpPost]
        public string CheckLogin(Person user, Token token)
        {
            AuthBL connect = new AuthBL();
            return JSONConvert.SerializeObject(connect.CheckIfUserLoggedIn(user, token));

            //List<Section> people = connect.GetSections();
            //var Tuser = user;
            //var JsonResult = Json(people, JsonRequestBehavior.AllowGet);
            //JsonResult.MaxJsonLength = int.MaxValue;
            //return JsonResult;
        }

        [Route("RegisterLogin")]
        [HttpPost]
        public string RegisterLogin(Person user)
        {
            AuthBL connect = new AuthBL();
            return JSONConvert.SerializeObject(connect.UserRegister(user));

            //return new ValidationResponse(true); 
            //List<Section> people = connect.GetSections();
            //var JsonResult = Json(people, JsonRequestBehavior.AllowGet);
            //JsonResult.MaxJsonLength = int.MaxValue;
            //return JsonResult;

        }
        [AuthorizationFilter]
        [Route("Logout")]
        [HttpPost]
        public bool Logout(Token token)
        {
            AuthBL auth = new AuthBL();
            return auth.RemoveToken(token);
        }


        
        [Route("GetSections")]
        [HttpGet]
        public string GetSections()
        {
            AuthBL connect = new AuthBL();
            List<Section> sections = connect.GetSections();
            return JSONConvert.SerializeObject(sections);
        }
        [AuthorizationFilter]
        [RoleSupervisor]
        [Route("AddSection")]
        [HttpPost]
        public string AddSection(string sectionName,string sectionAddress)
        {
            SectionBL connect = new SectionBL();
            return JSONConvert.SerializeObject(
                connect.Create(
                new Section
                {
                    CityName = sectionName,
                    Address = sectionAddress
                }
                ));
        }

        [AuthorizationFilter]
        [RoleSupervisor]
        [Route("RemoveSections")]
        [HttpPost]
        public string RemoveSections(int[] ids)
        {
            SectionBL connect = new SectionBL();
            return JSONConvert.SerializeObject(connect.DeleteManySections(ids));
        }
        [AuthorizationFilter]
        [RoleSupervisor]
        [Route("EditSections")]
        [HttpPost]
        public List<ValidationResponse> EditSections(Section[] sections)
        {
            SectionBL connect = new SectionBL();
            return connect.UpdateManySection(sections);
        }





        [AuthorizationFilter]
        [Route("GetUsers")]
        [HttpGet]
        public string GetUsers()
        {
            PersonBL connect = new PersonBL();
            return JSONConvert.SerializeObject(connect.GetPeople());
        }
        [RoleSupervisor]
        [AuthorizationFilter]
        [Route("RemoveUser")]
        [HttpPost]
        public List<ValidationResponse> RemoveUsers(int[] ids)
        {
            PersonBL connect = new PersonBL();
            return connect.RemovePeople(ids);
        }
        [AuthorizationFilter]
        [Route("EditUser")]
        [HttpPost]
        public List<ValidationResponse> EditUsers(Person[] users)
        {
            PersonBL connect = new PersonBL();
            return connect.UpdateManyUsers(users);
        }
        [AuthorizationFilter]
        [Route("GetPosters")]
        [HttpGet]
        public string GetPosters()
        {
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.GetPosters());
            return result;
        }
        [AuthorizationFilter]
        [Route("RemovePoster")]
        [HttpPost]
        public string RemovePoster(Poster poster)
        {
            PosterBL connect = new PosterBL();
            var result = connect.Delete(poster);
            ValidationResponse returnInfo = new ValidationResponse();
            if (result)
                returnInfo= new ValidationResponse(true);
            else
                returnInfo =  new ValidationResponse(false);
            return JSONConvert.SerializeObject(returnInfo);
        }
        [AuthorizationFilter]
        [Route("GetPostersCount")]
        [HttpGet]
        public int GetPostersCount()
        {
            PosterBL connect = new PosterBL();
            return connect.GetPosters().Count;
        }

        //[HttpGet]
        //public int GetPersonID(Person p)
        //{
        //    PosterBL connect = new PosterBL();
        //    PersonBL connectPerson = new PersonBL();
        //    foreach (Person man in connectPerson.GetPeople())
        //        if (man.Id == p.Id)
        //            p = man;
        //    return p.Id;
        //}
        [AuthorizationFilter]
        [Route("GetPostersPerson")]
        [HttpGet]
        public int GetPostersPerson(Person p)
        {
            PosterBL connect = new PosterBL();
            PersonBL connectPerson = new PersonBL();
            foreach (Person man in connectPerson.GetPeople())
                if (man.Id == p.Id)
                    p = man;
            return connect.GetPostersByPerson(p).Count;
        }
       
     
        [AuthorizationFilter]
        [Route("CheckIfLogin")]
        [HttpPost]
        public bool CheckIfLogin(Token token)
        {
            AuthBL auth = new AuthBL();
            bool result = auth.CheckToken(token);
           
            return result;
        }

        [AuthorizationFilter]
        [Route("AddPoster")]
        [HttpPost]
        public bool AddPoster(Poster poster)
        {
            //if(poster)
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connect = new PosterBL();
            poster.AuthorID = person.Id;
            
            bool result = connect.Create(poster);
            return result;
        }
        [Route("AddPersonToPoster")]
        [HttpPost]
        [AuthorizationFilter]
        public string AddPersonToPoster(Poster poster)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.AddPassengerToPoster(person,poster));
            return result;
        }

        [Route("RemovePersonFromPoster")]
        [HttpPost]
        [AuthorizationFilter]
        public string RemovePersonFromPoster(Poster poster)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.RemovePassengerFromPoster(person, poster));
            return result;
        }
        [Route("CheckIfPersonBelongsToGroup")]
        [HttpPost]
        [AuthorizationFilter]
        public string CheckIfPersonBelongsToGroup(Poster poster)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.CheckIfPersonBelongsToGroup(person.Id, poster.Id));
            return result;
        }
        [Route("GetGroupByPoster")]
        [HttpPost]
        [AuthorizationFilter]
        public string GetGroupByPoster(Poster poster)
        {
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.GetGroupsByPosterID(poster.Id));
            return result;
        }
        
        [Route("PostersCountInfo")]
        [HttpPost]
        [AuthorizationFilter]
        public string PostersCountInfo()
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PosterBL connect = new PosterBL();
            var result = JSONConvert.SerializeObject(connect.PostersCountInfo(person));
            return result;
        }

        [Route("GetUserByToken")]
        [HttpPost]
        [AuthorizationFilter]
        public string GetUserByToken()
        {
            Token token = ReturnTokenObject();
            AuthBL connect = new AuthBL();
            if (token == null) return "Error";
            Token t = connect.returnToken(token);
            return JSONConvert.SerializeObject(t.Person1);
        }

        [Route("ChangeUserPassword")]
        [HttpPost]
        [AuthorizationFilter]
        public ValidationResponse ChangeUserPassword(string oldPassword, string newPassword)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PersonBL connect = new PersonBL();
            return connect.ChangeUserPassword(person, oldPassword, newPassword);
        }

        [Route("ChangeUserSection")]
        [HttpPost]
        [AuthorizationFilter]
        public ValidationResponse ChangeUserSection(int section)
        {
            Person person = JsonConvert.DeserializeObject<Person>(GetUserByToken());
            PersonBL connect = new PersonBL();
            person.Section = section;
            return connect.Update(person);
        }
        private Token ReturnTokenObject()
        {
            Token token = new Token()
            {
                TokenValue = Request.Headers.GetValues("Authorization")[0]
            };
            return token;
        }
    }
}