﻿using Billcar.BL;
using Billcar.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
namespace WebTest.Controllers
{
    public class AuthorizationFilter : ActionFilterAttribute
    {
        //protected string Username { get; set; }
        //protected string Password { get; set; }
        //public AuthorizationFilter(string username, string password)
        //{
        //    this.Username = username;
        //    this.Password = password;
        //}
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {


            var req = filterContext.HttpContext.Request;
            var auth = req.Headers["Authorization"];
            if (!String.IsNullOrEmpty(auth))
            {
                AuthBL authConnect = new AuthBL();
                Token t = new Token();
                t.TokenValue = auth;
                if (authConnect.CheckToken(t))
                {
                    return;
                }
            }
            filterContext.Result = new HttpUnauthorizedResult("Unauthorized");
        }

    }
    public class RoleSupervisor : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var req = filterContext.HttpContext.Request;
            var auth = req.Headers["Authorization"];
            if (!String.IsNullOrEmpty(auth))
            {
                AuthBL authConnect = new AuthBL();
                Token t = new Token();
                t.TokenValue = auth;
                if (authConnect.CheckToken(t))
                {
                    Token data = authConnect.returnToken(t);
                    if(data.Person1.Flags==true)
                        return;
                }
            }
            filterContext.Result = new HttpUnauthorizedResult("Unauthorized");
        }

    }
}