app.config(["$stateProvider", "$urlRouterProvider", "$ocLazyLoadProvider", "$locationProvider", function ($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, $locationProvider) {
    $locationProvider.hashPrefix("");
    $ocLazyLoadProvider.config({
        debug: false,
        events: true
    });

    $urlRouterProvider.otherwise("/login");

    $stateProvider
        .state("app",
            {
                url: "/"
            })
        .state("app.dashboard",
            {
                url: "",
                templateUrl: MAIN_FOLDER + "views/dashboard/main.html",
                resolve: {
                    loadMyDirectives: function($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            {
                                name: "sbAdminApp",
                                files: [
                                    MAIN_FOLDER + "scripts/directives/_headers/headerUser/headerUser.js",
                                    MAIN_FOLDER + "scripts/directives/_sidebars/sidebarPosters/sidebarPosters.js",
                                    MAIN_FOLDER + "scripts/directives/_sidebars/sidebarSettings/sidebarSettings.js"
                                ]
                            });
                    }
                }


            })


        // MINE
        .state("app.dashboard.addPoster",
            {
                //templateUrl: MAIN_FOLDER + 'views/user/addPoster.html',
                url: "posters/add",

                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/user/addPoster.html"
                    }

                },
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/user/posters/add.js"
                            ]
                        });
                    }
                }
            })
        .state("app.dashboard.listPosters",
            {
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/user/listPosters.html"
                    }

                },
                //templateUrl: MAIN_FOLDER + 'views/user/listPosters.html',
                url: "posters/list",
                params: {
                    menu: "hidden"
                },
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/user/posters/list.js"
                            ]
                        });
                    }
                }
            })
        .state("app.dashboard.aboutPosters",
            {
                //templateUrl: MAIN_FOLDER + 'views/user/aboutPosters.html',
                url: "posters/info",
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/user/aboutPosters.html"
                    }

                },
                resolve: {
                    loadMyFiles: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/user/posters/info.js",
                                MAIN_FOLDER + "scripts/directives/dashboard/stats/stats.js"
                            ]
                        });
                    }
                }
            })
        .state("app.dashboard.mapPosters",
            {
                templateUrl: MAIN_FOLDER + "views/user/mapPosters.html",
                url: "posters/map"
            })
        .state("app.dashboard.accountOptions",
            {
                url: "account",
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/user/accountOptions.html",
                    }
                },
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/user/account.js"
                            ]
                        });
                    }
                },
                params: {
                    menu: "settings"
                }
            })
        .state("app.supervisor",
            {
                url: "supervisor/",
                templateUrl: MAIN_FOLDER + "views/dashboard/main.html",
                resolve: {
                    LoadSupervisorMenu: function($ocLazyLoad) {
                        return $ocLazyLoad.load(
                            {
                                name: "sbAdminApp",
                                files: [
                                    MAIN_FOLDER + "scripts/directives/_headers/headerSupervisor/headerSupervisor.js",
                                    MAIN_FOLDER + "scripts/directives/_sidebars/sidebarSupervisor/sidebarSupervisor.js",
                                    MAIN_FOLDER + "scripts/directives/_sidebars/sidebarSSettings/sidebarSSettings.js"
                                ]
                            });
                    }
                }
            })
        .state("app.supervisor.accountOptions",
            {
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/supervisor/accountOptions.html",
                    }
                },
                
                url: "account",
                params: {
                    menu: "settings"
                }
            })
        .state("app.supervisor.manageAccounts",
            {
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/supervisor/manageAccounts.html",
                    }
                },
                
                url: "users",
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/supervisor/manageAccounts.js"
                            ]
                        });
                    }
                }
            })
        .state("app.supervisor.manageSections",
            {
                url: "sections",
                views: {
                    'Test': {
                        templateUrl: MAIN_FOLDER + "views/supervisor/manageSections.html",
                    }
                },
                
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/supervisor/manageSections.js"
                            ]
                        });
                    }
                }
            })
        .state("app.dashboard.blank",
            {
                templateUrl: MAIN_FOLDER + "views/blank.html",
                url: "blank"
            })
        .state("app.login",
            {
                templateUrl: MAIN_FOLDER + "views/login.html",
                url: "login",
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/login.js"
                            ]
                        });
                    }
                },
                params: {
                    authorization: false
                }
            })
        .state("app.register",
            {
                templateUrl: MAIN_FOLDER + "views/register.html",
                url: "register",
                resolve: {
                    loadCtrl: function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: "sbAdminApp",
                            files: [
                                MAIN_FOLDER + "scripts/controllers/register.js"
                            ]
                        });
                    }
                },
                params: {
                    authorization: false
                }
            });

}])


