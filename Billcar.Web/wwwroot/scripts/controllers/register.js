﻿'use strict';
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module('sbAdminApp')
  .controller('ReigsterCtrl', function ($scope, $http) {
      console.log("RegisterCtrl run");
      $scope.checkRegister = function () {
          if ($scope.registerForm.inputEmailName.$valid && $scope.registerForm.inputPasswordName.$valid && $scope.user.Email.indexOf(".") > 0 && $scope.user.Email.indexOf("@") > 0 && $scope.registerForm.inputNameName.$valid && $scope.registerForm.inputSurnameName.$valid && $scope.registerForm.inputSectionName.$valid && $scope.registerForm.inputPasswordRepeatName.$valid && $scope.registerForm.inputPasswordRepeatName.$valid == $scope.registerForm.inputPasswordName.$valid) {
              
              return false;
          }
          else {
              return true;
          }
      };
      $scope.registerFormButton = function (user) {
          console.log("posting data....");
          console.log(user);
          $http({
              method: "POST", url: "/api/RegisterLogin", dataType: 'json',
              headers: {  "Content-Type": "application/json" },
              params: user
          }).then(function (response, status) {
              console.log("Response register in");
              console.log(response);
              console.log(response.data);
              console.log(response.data.Successful);
              $scope.errorRegisterMessage = "";
              $scope.successRegisterMessage = "";
              if (response.data.Successful == true) {
                  console.log("OK, created");
                  $scope.successRegisterMessage = response.data.Information;
                  //$state.go('root.auth.login', {}, { reload: true });
              }
              else {
                  $scope.errorRegisterMessage = response.data.Information;
              }
          }, function errorCallback(response) {
              console.log("ERROR response:");
              console.log(response);
          });
      };
      $http({
          method: "GET", url: "/api/GetSections", dataType: 'json',
          headers: { "Content-Type": "application/json" }
      }).then(function (response, status) {
          console.log("/api/GetSecions OK");
          $scope.sections = [];
          $scope.sections = response.data;
          $scope.loadingSection = true;
      }, function errorCallback(response) {
          console.log("ERROR response:");
          console.log(response);
      });
  });
