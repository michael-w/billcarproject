﻿"use strict";
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module("sbAdminApp")
  .controller("LoginCtrl", function ($scope,Authorization) {
      console.log("LoginCtrl run");
      $scope.loginFormButton = function (userForm) {
          
            var response = Authorization.login(userForm);
            response.then(function (result) {
                console.log("RESPONSE !!!");
                console.log(result);
                $scope.errorLoginMessage = "";
                $scope.successRegisterMessage = "";
                if (result.Successful) {
                    $scope.successRegisterMessage = "Przekierowanie...";
                } else {
                    $scope.errorLoginMessage = result.Information;
                }
            });
      }
    
      
    });
