'use strict';

angular.module('sbAdminApp')
    .controller("ListPosters", ['$rootScope', '$http', '$scope', '$cookies', 'NgMap', 'PosterService', 'orderByFilter', function ($rootScope, $http, $scope, $cookies, NgMap, PosterService, orderBy) {
        console.log("ListPosters");

        $scope.sortName = null;
        $scope.reverse = false;
        var reloadPosters = function () {
            $http({
                method: "GET", url: "/api/GetPosters", dataType: 'json',
                headers: { "Content-Type": "application/json" }
            }).then(function (response, status) {
                console.log("OK repsonse GetPosters");
                $scope.posters = [];

                angular.forEach(response.data, function (value, key) {
                    var date = moment(value.TransportDate, 'HH:mm DD-MM-YYYY');
                    var now = moment();
                    if (date > now) {
                        this.push(value);
                    }
                }, $scope.posters);
                console.log($scope.posters);
                $scope.posters = orderBy($scope.posters, "TransportDate", true, sortDate);
                $scope.sortName = "TransportDate";

            });
        }
        reloadPosters();
        $scope.isUserInPoster = function (array, id) {
            var exist = false;
            angular.forEach(array, function (value, key) {
                if (value.PersonID == id) {
                    exist = true;
                }
            });
            return exist;
        }

        console.log("SUER ID:::");
        console.log($rootScope.userId);
       

        $scope.$on('mapInitialized', function (event, m) {
            NgMap.getMap("my-map").then(function (map) {
                console.log("INITALIZED");
                $scope.mapLoaded = true;
                google.maps.event.trigger(map, 'resize');
            });

        });
        var refreshMap = function (mapName, timeout) {
            $scope.loadedPage = true;
            if (mapName) {
                setTimeout(function () {
                    setTimeout(function () {
                        NgMap.getMap(mapName).then(function (map) {
                            google.maps.event.trigger(map, 'resize');
                        });
                    },
                            timeout);
                },
                    1);
            } else {

                setTimeout(function () {
                    setTimeout(function () {
                        NgMap.getMap().then(function (map) {
                            google.maps.event.trigger(map, 'resize');

                        });
                    },
                            timeout);
                },
                    1);
            }

        }
        $scope.showDetails = function (p) {
            $scope.loadedPage = false;
            $scope.selectedPoster = p;
            refreshMap(null, 340);
        }
        $scope.removeDetails = function () {
            reloadPosters();
            $scope.selectedPoster = null;
            refreshMap(null, 10);
            
        }


        $scope.removePoster = function (p) {
            var result = PosterService.removePoster(p);
            result.then(function (response) {
                if (response.Successful == true) {
                    for (var i = $scope.posters.length - 1; i >= 0; i--) {
                        if ($scope.posters[i].Id == p.Id)
                            $scope.posters.splice(i, 1);
                    }
                    $scope.selectedPoster = undefined;
                }
            });


        }
        $scope.getNumber = function (num) {
            return new Array(num);
        }

        var sortDate = function (date1, date2) {
            var date1 = moment(date1.value, 'HH:mm DD-MM-YYYY').toDate();
            var date2 = moment(date2.value, 'HH:mm DD-MM-YYYY').toDate();
            if (date1 > date2) {
                return -1;
            } else {
                return 1;
            }
        };
        var sortAuthor = function (date1, date2) {
            console.log(date1.value);
            if (date1.type == "number") return 0;
            if (date1.value.indexOf(":") >= 0) {
                var result = sortDate(date1, date2);
                if (result == true) result = false;
                return result;
            } else {
                return date1.value.localeCompare(date2.value);
            }



        };

        $scope.sortBy = function (propertyName) {
            $scope.sortName = propertyName;
            // Sortowanie po kierunku
            if (propertyName === "Direction") {
                if ($scope.reverse) {
                    propertyName = ["Direction", "Section.CityName", "-To"];
                } else {
                    propertyName = ["Direction", "Section.CityName", "+To"];
                }
                $scope.posters = orderBy($scope.posters, propertyName, $scope.reverse);
            }
            if (propertyName === "TransportDate") {
                $scope.posters = orderBy($scope.posters, propertyName, $scope.reverse, sortDate);
            }
            if (propertyName === "Person") {
                propertyName = ["Person.FirstName", "Person.LastName", "TransportDate"];
                $scope.posters = orderBy($scope.posters, propertyName, $scope.reverse, sortAuthor);
            }
            if (propertyName === "Section.CityName") {
                propertyName = ["Person.FirstName", "Person.LastName", "TransportDate"];
                $scope.posters = orderBy($scope.posters, propertyName, $scope.reverse, sortAuthor);
            }

            $scope.reverse = !$scope.reverse;

        };


        //$scope.checkifUserInPoster = function (p) {


        //    var result = PosterService.checkifUserInPoster(p);
        //    result.then(function (response) {
        //        if (response.Successful == true) {
        //            for (var i = $scope.posters.length - 1; i >= 0; i--) {
        //                if ($scope.posters[i].Id == p.Id)
        //                    $scope.posters.splice(i, 1);
        //            }
        //        } else {

        //        }
        //    });


        //}

        $scope.joinPoster = function (poster) {
            var result = PosterService.AddPersonToPoster(poster);
            result.then(function (response) {
                if (response.Successful == true) {
                    var pushNewPerson = {
                        Id: -1,
                        PersonID: $scope.userId,
                        PosterID: poster.Id,
                        Person: {FirstName:$scope.userName}
                    }
                    $scope.selectedPoster.Group.push(pushNewPerson);
                    $scope.selectedPoster.TransportCurrSize++;
                    

                    refreshMap(null, 340);
                } else {
                }
            });
        }
        $scope.exitFromPoster = function (poster) {
            var result = PosterService.RemovePersonFromPoster(poster);
            result.then(function (response) {
                if (response.Successful == true) {
                    $scope.selectedPoster.Group = $scope.selectedPoster.Group.filter(function (obj) {
                        return obj.PersonID !== $scope.userId;
                    });
                    $scope.selectedPoster.TransportCurrSize--;
                    refreshMap(null, 340);
                } else {
                }
            });
        }
        $scope.isDateMoreThan24H= function(data) {
            var date = moment(data, 'HH:mm DD-MM-YYYY').toDate();
            var now = moment().add(1, 'days');
            if (date > now) {
                return true;
            } else {
                return false;
            }
        }


    }]);
