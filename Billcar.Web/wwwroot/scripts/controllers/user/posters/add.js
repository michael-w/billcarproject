"use strict";
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */

angular.module("sbAdminApp")
    .controller("AddPoster", ["$rootScope", "$http", "$scope", "PosterService", "NgMap", function ($rootScope, $http, $scope, PosterService, NgMap) {
        console.log("AddPoster");

        $scope.result1 = '';
        $scope.options1 = null;
        $scope.details1 = '';


        var vm = this;
        vm.poster = {};

        vm.mindate = moment();
        vm.maxdate = moment().add(30, 'day');
        vm.addPoster = function () {
            if (vm.poster.TransportDate == "Invalid date") return;
            vm.poster.From = vm.poster.From.Id;
            console.log("addposter call");
            console.log(vm.poster.TransportDate);
            vm.poster.TransportDate = moment(new Date(vm.poster.TransportDate)).format("HH:mm DD-MM-YYYY");
            var result = PosterService.addPoster(vm.poster);
            result.then(function (response) {
                console.log("ODP:");
                console.log(response);
                if (response == "True")
                    $scope.posterAdded = 1;
                else 
                    $scope.posterAdded = 2;
            });


        }
        //var mainMap;
        //NgMap.getMap("my-map").then(function (map) {
        //    console.log(map);
        //    $scope.mapLoaded = true;
        //    //map = NgMap.initMap("my-map");
        //    google.maps.event.trigger(map, 'resize');
        //});
        $scope.$on('mapInitialized', function (event, m) {
            //mainMap = m;
            console.log("HEJ");
            //$scope.showMap = 2;
            //vm.map = NgMap.initMap("map");
            NgMap.getMap("my-map").then(function (map) {
                console.log(map);
                $scope.mapLoaded = true;
                //map = NgMap.initMap("my-map");
                google.maps.event.trigger(map, 'resize');
            });

            $scope.userSectionDetails = {};
            $http({
                method: "GET",
                url: "/api/GetSections"
            }).then(function (response) {
                console.log("TEST response:");
                $scope.sections = [];
                $scope.sections = response.data;
                //debugger;
                for (var i = 0; i < response.data.length; i++) {
                    if ($rootScope.userSection == response.data[i].CityName) {
                        $scope.userSectionDetails = response.data[i];
                        NgMap.getMap("my-map").then(function (map) {
                            codeAddress($scope.userSectionDetails.Address, map);
                            console.log("SHOW;");
                            console.log(vm.poster.From);
                            console.log("SHOW;");
                            vm.poster.From = $scope.userSectionDetails;
                            console.log("SHOW;");
                            console.log(vm.poster.From);
                        });
                    }
                }

            });
        });

        var codeAddress = function (address, map) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address },
                function(results, status) {
                    if (status == 'OK') {
                        map.setCenter(results[0].geometry.location);
                       
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                })
        };
        
        $scope.showMarkerLatlng = function () {
            NgMap.getMap().then(function (map) {
                //console.log(map.directionsRenderers[0].directions.routes[0].legs[0]);
                //console.log(map.directionsRenderers[0].directions.routes[0].legs[0].start_address);
                //console.log(map.directionsRenderers[0].directions.routes[0].legs[0].end_address);
                //console.log(map.directionsRenderers[0].directions.routes[0].legs[0].via_waypoint);
                //for (i = 0; i < map.directionsRenderers[0].directions.routes[0].legs[0].via_waypoint.length; i++) {
                //    var point = map.directionsRenderers[0].directions.routes[0].legs[0].via_waypoint[i];
                //    console.log(point);
                //    //var step = map.directionsRenderers[0].directions.routes[0].legs[0].steps[index];
                //};
            });

        }
        $scope.wayPoints = [
            //{location: {lat:44.32384807250689, lng: -78.079833984375}},
            // {location: {lat:44.55916341529184, lng: -76.17919921875}},
        ];



        vm.returnToAddPoster = function () {
            vm.poster = {};
            $scope.posterAdded = false;
        }
        $scope.$watch('vm.poster.From',
            function () {
                console.log(vm.poster.From);
            });
        $scope.$watch('vm.poster.Direction', function () {
            
            vm.poster.To = null;
            vm.poster.From.Address = null;
            $scope.$apply();
            
            //NgMap.getMap("my-map").then(function (map) {
            //    console.log(map);
            //    $scope.mapLoaded = true;
            //    //map = NgMap.initMap("my-map");
            //    google.maps.event.trigger(map, 'resize');
            //});
            //map.directionsDisplay.setDirections({ routes: [] });
        });
        //$scope.addPoster = function (poster) {
        //    console.log(poster);
        //    var newDate = moment(new Date(poster.data)).format("HH:mm DD-MM-YYYY");
        //    console.log(newDate);
          
        //}
       



    }]);
