"use strict";
/**
 * @ngdoc function
 * @name sbAdminApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the sbAdminApp
 */
angular.module("sbAdminApp")
    .controller("ManageAccounts", ["$http", "$scope", "$q", "$window", function ($http, $scope, $q, $window) {
        console.log("ManageAccounts");
        $scope.enableSaveButton = function () {
            $scope.notChanges = 1;
        }
        var removedPeople = [];
        var setSections = function (people, sections) {
            for (var i in people) {
                if (people.hasOwnProperty(i)) {
                    for (var x in sections) {
                        if (sections.hasOwnProperty(x)) {
                            if (people[i].Section === sections[x].Id) {
                                people[i].Section = sections[x].CityName;
                            }
                        }
                    }
                }
            }
            return people;
        };
        var setPosters = function (people, posters) {
            for (var i in people) {
                if (people.hasOwnProperty(i)) {
                    people[i].PostersCount = 0;
                    for (var x in posters) {
                        if (posters.hasOwnProperty(x)) {
                            if (people[i].Id === posters[x].AuthorID) {
                                people[i].PostersCount++;
                            }
                        }
                    }
                }
            }
            return people;
        };


        $q.all([$http({
            method: "GET",
            url: "/api/GetSections"
        }).then(function (response) {
            console.log("OK repsonse GetSections");
            $scope.sections = [];
            $scope.sections = response.data;
            
        }),
    $http({
        method: "GET",
        url: "/api/GetUsers"
    }).then(function (response) {
        console.log("OK repsonse GetPeople");
        $scope.people = [];
        $scope.people = response.data;
    }),$http({
        method: "GET", url: "/api/GetPosters", dataType: "json",
        headers: { "Content-Type": "application/json" }
    }).then(function (response) {
        console.log("OK repsonse GetPosters");
        $scope.posters = [];
        $scope.posters = response.data;
    })
        ]).then(function () {
            console.log("OK all");
            console.log($scope.people[0].Section);
       // $scope.people = setSections($scope.people, $scope.sections);
        $scope.people = setPosters($scope.people, $scope.posters);
        console.log($scope.people);
    }, function errorCallback(response) {
        console.log("ERROR");
        console.log(response);
    });
    
        $scope.removeUser = function (Id) {
            
            $scope.enableSaveButton();
            for (var i in $scope.people) {
                if ($scope.people[i].Id === Id) {
                    removedPeople.push($scope.people[i].Id);
                    $scope.people[i].removed = true;
                    console.log("ADD");
                    //$scope.people.splice([i], 1);
                }
            }
        };
        
        $scope.saveChanges = function () {
            console.log("Removing user id...");
            
                console.log("... : " + removedPeople);
                    $http({
                    method: "POST",
                    url: "/api/RemoveUser", dataType: "json",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: { ids: removedPeople }
                }).then(function () {
                    console.log("OK remowed " + removedPeople);
                    console.log($scope.people);
                    
                    for (var i = $scope.people.length - 1; i >= 0; i--) {
                            if ($scope.people[i].removed) {
                                $scope.people.splice([i], 1);
                            }
                    }
                    
                    $http({
                        method: "POST",
                        url: "/api/EditUser", dataType: "json",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: { users :$scope.people }
                    }).then(function () {
                        console.log("OK edited ");
                        
                    })
                    
                })
               
                
            //    for (var x in $scope.people) {
            //        if ($scope.people[x].Id == removedPeople) {
            //            $scope.people.splice([x], 1);
            //        }
            //    }
            
            //for (var i in $scope.people) {
            //    console.log("... : " + removedPeople[i]);
            //    $http({
            //        method: "POST",
            //        url: "/api/EditUser", dataType: 'json',
            //        headers: {
            //            "Content-Type": "application/json",
            //        },
            //        data: { id: removedPeople[i] }
            //    }).then(function (response, status) {
            //        console.log("OK remowed " + removedPeople[i]);
            //    })
            //}

        };
   
   
    

}]);
