'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
    .directive('sidebarSettings', ['Authorization', '$location', function (Authorization, $location) {
        return {
            templateUrl: MAIN_FOLDER + 'scripts/directives/_sidebars/sidebarSettings/sidebarSettings.html',
            restrict: 'E',
            replace: true,
            controller: function ($scope) {
                $scope.logout = function () {
                    console.log(Authorization);
                    Authorization.logout();
                };
            }
        }
    }]);
