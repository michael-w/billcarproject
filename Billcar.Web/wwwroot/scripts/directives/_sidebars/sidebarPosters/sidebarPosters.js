'use strict';

/**
 * @ngdoc directive
 * @name izzyposWebApp.directive:adminPosHeader
 * @description
 * # adminPosHeader
 */

angular.module('sbAdminApp')
  .directive('sidebar',['$location',function() {
    return {
        templateUrl: MAIN_FOLDER + 'scripts/directives/_sidebars/sidebarPosters/sidebarPosters.html',
      restrict: 'E',
      replace: true
      
    }
  }]);
