//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Billcar.Core.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Poster
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Poster()
        {
            this.Group = new HashSet<Group>();
        }
    
        public int Id { get; set; }
        public int AuthorID { get; set; }
        public string TransportDate { get; set; }
        public int From { get; set; }
        public string To { get; set; }
        public bool Direction { get; set; }
        public int TransportMaxSize { get; set; }
        public int TransportCurrSize { get; set; }
        public Nullable<int> GroupID { get; set; }
    
        public virtual Person Person { get; set; }
        public virtual Section Section { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Group> Group { get; set; }
    }
}
