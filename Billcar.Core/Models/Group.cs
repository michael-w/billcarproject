//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Billcar.Core.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Group
    {
        public int Id { get; set; }
        public int PosterID { get; set; }
        public int PersonID { get; set; }
    
        public virtual Poster Poster { get; set; }
        public virtual Person Person { get; set; }
    }
}
